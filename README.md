CSharpStringEscaper
===================

A web form to convert an arbitrary string to an escaped C# string that can be used in source code. 

From a [StackOverflow Answer](http://stackoverflow.com/a/324812)

Assembled by [Josh Rivers](http://www.joshrivers.me) of [Recursive Erudition](http://radianttiger.com)


